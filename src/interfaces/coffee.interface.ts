export interface Coffee {
  variety: string;
  specie: string;
  origin: string;
}