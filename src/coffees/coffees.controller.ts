import { Controller, Get, Post, Body, Param, Query } from '@nestjs/common';
import { CoffeesService } from './coffees.service';
import { Coffee } from 'src/interfaces/coffee.interface';
import { CreateCoffeeDto, ListAllEntities } from './create-coffee.dto';

@Controller('coffees')
export class CoffeesController {
  constructor(private coffeeService: CoffeesService) { }

  @Post()
  async create(@Body() createCoffeeDto: CreateCoffeeDto) {
    this.coffeeService.create(createCoffeeDto);
  }

  @Get()
  findAll(@Query() query: ListAllEntities): Coffee[] {
    return this.coffeeService.findAll(query);
  }

  @Get(':variety')
  findByVariety(@Param('variety') variety: string): Coffee[] {
    return this.coffeeService.findByVariety(variety);
  }
}
