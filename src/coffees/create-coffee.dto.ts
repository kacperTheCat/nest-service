export class CreateCoffeeDto {
  variety: string;
  specie: string;
  origin: string;
}

export class ListAllEntities {
  origin: string;
  limit: number;
  page: number;
}