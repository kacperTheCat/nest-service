import { Injectable } from '@nestjs/common';
import { Coffee } from '../interfaces/coffee.interface';
import { ListAllEntities } from './create-coffee.dto';
import { paginate, Pagination, IPaginationOptions } from 'nestjs-typeorm-paginate';

@Injectable()
export class CoffeesService {
  private readonly coffees: Coffee[] = [];

  create(coffee: Coffee) {
    this.coffees.push(coffee);
  }

  findAll(query: ListAllEntities): Coffee[] {
    return this.coffees.filter(coffee =>
      coffee.origin === query.origin);
  }

  findByVariety(variety: string): Coffee[] {
    return this.coffees.filter(coffee =>
      coffee.variety === variety);
  }
}