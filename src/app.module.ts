import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CoffeesController } from './coffees/coffees.controller';
import { CoffeesService } from './coffees/coffees.service';
import { CoffesController } from './coffes/coffes.controller';

@Module({
  imports: [],
  controllers: [AppController, CoffeesController, CoffesController],
  providers: [AppService, CoffeesService],
})
export class AppModule {}
